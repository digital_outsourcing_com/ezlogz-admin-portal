package com.ezlogz.webviewez;

import android.app.Application;
import android.webkit.WebView;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

public class Ezlogz extends Application {
    private static Bus mEventBus;

    public static Bus getBus() {
        return mEventBus;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        WebView.setWebContentsDebuggingEnabled(true);
        mEventBus = new Bus(ThreadEnforcer.ANY);
    }
}
