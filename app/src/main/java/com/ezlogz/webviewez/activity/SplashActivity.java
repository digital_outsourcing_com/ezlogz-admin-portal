package com.ezlogz.webviewez.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ezlogz.webviewez.utils.ActivityUtils;
import com.ezlogz.webviewez.R;
import com.ezlogz.webviewez.fragment.SplashFragment;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActivityUtils.replaceFragmentToActivity(getSupportFragmentManager(), new SplashFragment(), R.id.container);
    }
}
