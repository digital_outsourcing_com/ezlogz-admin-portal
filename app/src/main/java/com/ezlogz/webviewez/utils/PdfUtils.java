package com.ezlogz.webviewez.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.ezlogz.webviewez.Constants;

public class PdfUtils {
    public static void startPdfViewer(Activity context, String url) {
        Uri path = Uri.parse(url);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        try {
            context.startActivityForResult(pdfIntent, 100);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No PDF application found", Toast.LENGTH_SHORT).show();
            String pdfUrl = Constants.PDF_VIEWER + url + "&embedded=true";
            startPdfToChrome(context, pdfUrl);
        }
    }

    public static boolean startPdfToChrome(Activity context, String url) {
        Intent intent = new Intent();
        intent.setPackage("com.android.chrome");
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        try {
            context.startActivityForResult(intent, 100);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No PDF application found", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
