package com.ezlogz.webviewez.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class ActivityUtils {

    public static void startActivityForResult(Activity activity, Class classActivity, int requestCode) {
        Intent intent = new Intent(activity, classActivity);
        activity.startActivityForResult(intent, requestCode);
    }

    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, int frameId) {
        try {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(frameId, fragment);
            transaction.commit();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void replaceFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                                 @NonNull Fragment fragment, int frameId) throws NullPointerException {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment);
        transaction.commit();
    }

    public static void replaceFragmentToActivityWithBackStack(@NonNull FragmentManager fragmentManager,
                                                              @NonNull Fragment fragment, int frameId) {
        try {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(frameId, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void replaceFragmentToChildFragment(@NonNull FragmentManager fragmentManager,
                                                      @NonNull Fragment fragment, int frameId) {
        try {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(frameId, fragment);
            transaction.commit();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void hideSoftInputFromWindow(Activity activity) {
        if (activity == null || activity.getCurrentFocus() == null)
            return;
        InputMethodManager ex = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert ex != null;
        ex.hideSoftInputFromWindow(activity.getCurrentFocus()
                .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void showSoftInputFromWindow(Activity activity, EditText edit) {
        if (activity == null || activity.getCurrentFocus() == null)
            return;

        InputMethodManager ex = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert ex != null;
        ex.showSoftInput(edit, InputMethodManager.SHOW_IMPLICIT);
    }
}
