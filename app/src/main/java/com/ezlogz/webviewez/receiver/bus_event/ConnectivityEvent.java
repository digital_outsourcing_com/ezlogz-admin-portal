package com.ezlogz.webviewez.receiver.bus_event;

public class ConnectivityEvent {
    public String status;
    public boolean isStatus;

    public ConnectivityEvent(String status, boolean isStatus) {
        this.status = status;
        this.isStatus = isStatus;
    }
}
