package com.ezlogz.webviewez.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ezlogz.webviewez.Constants;
import com.ezlogz.webviewez.Ezlogz;
import com.ezlogz.webviewez.receiver.bus_event.ConnectivityEvent;
import com.ezlogz.webviewez.utils.NetworkUtils;

public class ConnectivityChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String status = NetworkUtils.getConnectivityStatusString(context);
        if (status.contains(Constants.NOT_CONNECT)) {
            Ezlogz.getBus().post(new ConnectivityEvent(status, false));
        } else {
            Ezlogz.getBus().post(new ConnectivityEvent(status, true));
        }
    }
}
