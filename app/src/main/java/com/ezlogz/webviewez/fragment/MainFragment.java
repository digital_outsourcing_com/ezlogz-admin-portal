package com.ezlogz.webviewez.fragment;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.ezlogz.webviewez.BuildConfig;
import com.ezlogz.webviewez.Constants;
import com.ezlogz.webviewez.Ezlogz;
import com.ezlogz.webviewez.R;
import com.ezlogz.webviewez.receiver.bus_event.ConnectivityEvent;
import com.ezlogz.webviewez.utils.NetworkUtils;
import com.ezlogz.webviewez.utils.PdfUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class MainFragment extends Fragment {

    private Bus mEventBus = Ezlogz.getBus();

    private ProgressBar mProgress;
    private WebView mWebView;
    private RelativeLayout mRlNoInternet;
    private boolean mNetworkStateChanged;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNetworkStateChanged = NetworkUtils.getConnectivityStatusBoolean(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        if (view != null) {
            initUI(view);
        }
        return view;
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initUI(View view) {
        mProgress = view.findViewById(R.id.progress);
        mWebView = view.findViewById(R.id.web_view);
        mRlNoInternet = view.findViewById(R.id.rl_no_internet);

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);
        webSettings.setUserAgentString("ezlogz_dash_app");

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return false;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                mProgress.setVisibility(View.GONE);
                mRlNoInternet.setVisibility(mNetworkStateChanged ? View.GONE : View.VISIBLE);
                mWebView.setVisibility(mNetworkStateChanged ? View.VISIBLE : View.GONE);
            }
        });
        mWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                WebView newWebView = new WebView(getContext());
                WebSettings webSettings = newWebView.getSettings();
                webSettings.setJavaScriptEnabled(true);
                webSettings.setUseWideViewPort(true);

                newWebView.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                        if (request.getUrl().toString().endsWith("pdf")) {
                            PdfUtils.startPdfViewer(getActivity(), request.getUrl().toString());
//                            if (!PdfUtils.startPdfToChrome(getActivity(), pdfUrl))
//                                view.loadUrl(pdfUrl);
                        }
                        return false;
                    }

                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        if (url.endsWith("pdf")) {
                            PdfUtils.startPdfViewer(getActivity(), url);
//                            if (!PdfUtils.startPdfToChrome(getActivity(), pdfUrl))
//                                view.loadUrl(pdfUrl);
                        }
                        return false;
                    }
                });

                newWebView.setWebChromeClient(new WebChromeClient() {
                    @Override
                    public void onCloseWindow(WebView window) {
                    }
                });

                ((WebView.WebViewTransport) resultMsg.obj).setWebView(newWebView);
                resultMsg.sendToTarget();
                return true;
            }

        });
        mWebView.loadUrl(Constants.URL);
    }

    @Override
    public void onResume() {
        super.onResume();
        mEventBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mEventBus.unregister(this);
    }

    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            if (getActivity() != null)
                getActivity().finish();
        }
    }

    @Subscribe
    public void onConnectivityEvent(ConnectivityEvent connectivityEvent) {
        mNetworkStateChanged = connectivityEvent.isStatus;
        try {
            if (mNetworkStateChanged) {
                mWebView.reload();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
