package com.ezlogz.webviewez.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ezlogz.webviewez.utils.ActivityUtils;
import com.ezlogz.webviewez.Constants;
import com.ezlogz.webviewez.R;
import com.ezlogz.webviewez.activity.MainActivity;

public class SplashFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_splash, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    ActivityUtils.startActivityForResult(getActivity(), MainActivity.class, Constants.REQUEST_CODE);
                    getActivity().finish();
                }
            }
        }, 1000);
    }
}
