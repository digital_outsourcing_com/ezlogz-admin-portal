package com.ezlogz.webviewez;

public class Constants {

    public static final boolean isRelease = false;

    public static final int REQUEST_CODE = 100;

    public static final String PDF_VIEWER = "https://docs.google.com/viewer?url=";

    private static final String MAIN_SERVER = "https://ezlogz.com";

    private static final String DEV_SERVER = "https://dev.ezlogz.com";

    public static String URL;

    public static void setApi() {
        if (isRelease) URL = MAIN_SERVER;
        else URL = DEV_SERVER;
    }

    public static final String CONNECT_TO_WIFI = "WIFI";
    public static final String CONNECT_TO_MOBILE = "MOBILE";
    public static final String NOT_CONNECT = "NOT_CONNECT";
    public final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";

}
